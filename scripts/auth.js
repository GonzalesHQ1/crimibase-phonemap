var un = 'f11a2e2643a585edcc85ded3d86325c9';
var pd = '9e78a403cc6d3835538d1fe6bc64f551';
var tok = 'secritT0qin';

var map = $('#map-wrap');
var preloader = $('#preloader');

var logoutBtn = $('#logout-btn');
var loginForm = $('#login-form');
var isLoggedin = localStorage.getItem('token') === tok;


/**
 * 
 * Load templates
 */

function loadLoginForm() {
    //Call .loadTemplate() on the target container
    $('#target').loadTemplate(
        //Specify the template container (or file name of external template)
        $('#loginForm')
    );
    hideErrors();
}

function loadMap() {
    //Call .loadTemplate() on the target container
    $('#target').loadTemplate(
        //Specify the template container (or file name of external template)
        $('#map')
    );
    initMap();
}


/**
 * Login/out
 */

function loginIfTokenPresent() {
    if (localStorage.getItem('token') === tok) {
        logUserIn();
        return;
    }
    loadLoginForm();
}

function login() {

    hideErrors();

    var phone = $('#phone').val();
    var code = $('#paswoord').val();

    if (phone === '' || code === '') {
        showRequiredMsg();
        return;
    }

    if (compare(phone, code)) {
        logUserIn();
        hideErrors();
        storeToken();
    } else {
        showLoginErrorMsg();
    }
}


function logout() {
    localStorage.removeItem('token');
    initPage();
    hideErrors();
    hideLogoutButton();
    loadLoginForm();
}

function logUserIn() {
    showLogoutButton();
    loadMap();
}

/**
 * Error handling
 */

function hideErrors() {
    hideLoginErrorMsg();
    hideRequiredMsg();
}

function hideLoginErrorMsg(params) {
    var errorMsg = $('#error-msg');
    errorMsg.hide();
}

function showLoginErrorMsg(params) {
    var errorMsg = $('#error-msg');
    errorMsg.show();
}

function showRequiredMsg(params) {
    var requiredMsg = $('#error-msg-required');
    requiredMsg.show();
}

function hideRequiredMsg(params) {
    var requiredMsg = $('#error-msg-required');
    requiredMsg.hide();
}

/**
 * Utils
 */

function hideLogoutButton() {
    logoutBtn.hide();
}

function showLogoutButton() {
    logoutBtn.show();
}



// auth utils

function compare(num, code) {
    var encodedNum = md5(num);
    var encodedSecret = md5(code);

    return encodedNum === un && encodedSecret === pd;
}

function storeToken(params) {
    localStorage.setItem('token', tok);
}

/**
 * Init
 */

function initPage() {
    hideErrors();
}

$(document).ready(function () {
    initPage();
    hideLogoutButton();
    loginIfTokenPresent();
});