var lat = '50.878497';
var lon = '4.708992';
var zoom = '25';
var key = 'AIzaSyA9D79jdNE6wzEomo4nw-6tsozoY08K37I';
var mapUrl = 'https://maps.google.com/maps?key=${key}q=50.878497,4.708992&z=15&output=embed'
/**
 * Utils
 */

function constructUrl(lat, lon, zoom) {
    return `https://maps.google.com/maps?key=${key}&q=${lat},${lon}&z=${zoom}&output=embed`
}

// Err show/hide

function hideGeoError() {
    var geoErrorText = $('#geo-error-text');
    geoErrorText.hide();
}

function showGeoError() {
    var geoErrorText = $('#geo-error-text');
    geoErrorText.show();
}

function hidephoneFoundSuccess() {
    var phoneFoundSuccessText = $('#phone-found-text');
    phoneFoundSuccessText.hide();
}

function showphoneFoundSuccess() {
    hideGeoError();
    var phoneFoundSuccessText = $('#phone-found-text');
    phoneFoundSuccessText.show();
}

function hideMap() {
    var mapEl = $('#map');
    mapEl.hide();
}

function showMap() {
    var mapEl = $('#map');
    mapEl.show();
}

/**
 * Geolocation Navigator handlers
 */

function isGeoSupported() {
    return "geolocation" in navigator;
}

function onSuccessfulGetPosition(position) {
    console.log(position)
    showMap();
    var url = constructUrl(position.coords.latitude, position.coords.longitude, zoom);
    changeCoords(url);
}

function onFailedGetPosition(err) {
    console.log(err)
    console.log('failed to get user position');
    showGeoError();
    // var url = constructUrl(lat, lon, zoom);
    // changeCoords(url);
}

function getOwnCoords() {
    if (!isGeoSupported) {
        showMap();
        return;
    }
    navigator.geolocation.getCurrentPosition(onSuccessfulGetPosition, onFailedGetPosition, { enableHighAccuracy: false, timeout: 5000 });
}

/**
 * Changing map Coords
 */

function onClickChangeCoords() {
    var url = constructUrl(lat, lon, zoom)
    hideGeoError();
    showphoneFoundSuccess();
    changeCoords(url);
}

function changeCoords(url) {
    showMap();

    enterCoordsIntoMap(url);
}

function enterCoordsIntoMap(url) {
    $('#map').attr('src', url);
}

/**
 * Init
 */

// function Init() {
//     if (isLoggedin) {
//         hidephoneFoundSuccess();
//         hideGeoError();
//         // getOwnCoords();
//     }
// }

function initMap() {
    hidephoneFoundSuccess();
    hideGeoError();
    hideMap();

    if (isLoggedin) {
        hidephoneFoundSuccess();
        hideGeoError();
        getOwnCoords();
    }
}


// $(document).ready(function () {
//     Init();
// });